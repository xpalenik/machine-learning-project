# Prediction using Linear Regression

I am going to use three Linear Regression models to investigate the [Online News Popularity Data Set](http://archive.ics.uci.edu/ml/datasets/Online+News+Popularity). The datased contains 61 attributes. Only 6 variables proved to be relevant (important) for predictions in most models.

# 1. Introduction
## problem statement
I am going to use three **Linear Regression** models to investigate the **Online News Popularity Data Set**. The datased contains 61 attributes (a.k.a. variables, or factors), out of which 2 are non-predictive, another 2 will be proven unusable and 1 is the target (a.k.a. goal) variable. The target variable is called "shares" and represents "the number of shares in social networks (popularity)". Only 6 variables are in fact relevant (important) for predictions in most models, however, all variables will be used.


```R
library(caret)
news <- read.csv("news.csv", stringsAsFactors = T)
set.seed(123) # Setting the seed to make the split reproductible.
```

    Loading required package: lattice
    Loading required package: ggplot2
    

# 2. Exploratory Analysis
## data description and visualizations

I am visualizing a subset called "predictors" of features to be used for linear regression. The subset choice has been based on the used models.


```R
predictors <- c("kw_min_avg", 
              "kw_avg_avg",
              "kw_max_avg",
              "data_channel_is_entertainment",
              "self_reference_min_shares",
              "num_hrefs" 
              )
    
keywords <- c("kw_min_avg", 
              "kw_avg_avg",
              "kw_max_avg"
              )

featurePlot(x = news[, keywords], 
            y = news$shares, 
            plot = "scatter",
            labels = c("average keyword", "shares"),
            main = "Keywords",
            layout = c(3, 1)
            )
  
featurePlot(x = news[, "data_channel_is_entertainment"], 
            y = news$shares, 
            plot = "scatter",
            labels = c("channel is entertainment", "shares"),
            main = "Is the channel entertainment?"
            )

featurePlot(x = news[, "self_reference_min_shares"], 
            y = news$shares, 
            plot = "scatter",
            labels = c("referenced articles", "shares"),
            main = "Shares of referenced articles"
            )

featurePlot(x = news[, "num_hrefs"], 
            y = news$shares, 
            plot = "scatter",
            labels = c("number of links ", "shares"),
            main = "Number of links"
            )
```






    
![png](results_files/results_4_2.png)
    





    
![png](results_files/results_4_4.png)
    





    
![png](results_files/results_4_6.png)
    



    
![png](results_files/results_4_7.png)
    


# 3. Preprocessing

## Preprocessing phases

1. data splitting
2. preProcess on train set -> result is a model
3. apply model from 2. on train set using function predict
4. apply model from 2. on test set using function predict

## 3.1 Data splitting

Spliting training set into two parts based on outcome: 80% and 20% with the aim to avoid overfitting. The model is going to be trained on the (larger) training set and tested on the test set.


```R
news_size <- nrow(news)
smp_size <- floor(0.8 * news_size)
chosen_ind <- sample(seq_len(nrow(news)), size = smp_size)

train <- news[chosen_ind,]
test <- news[-chosen_ind,]
```

## 3.2 Centering and scaling


```R
sum(is.na(news))
```


0


There are no missing values. Therefore, no imputing (KNN) is required.


```R
preProcValues <- preProcess(train, method = c("center","scale"))
```

Citing the documentation,

method = "**center**" subtracts the mean of the predictor's data (again from the data in x) from the predictor values

while

method = "**scale**" divides by the standard deviation.

The "range" transformation scales the data to be within [0, 1]. If new samples have values larger or smaller than those in the training set, values will be outside of this range.
Predictors that are not numeric are ignored in the calculations.

source: R Documentation for CARET


```R
train_processed <- predict(preProcValues, train)
quantile(train_processed$shares)  

test_processed <- predict(preProcValues, test)
quantile(test_processed$shares)  
```


<dl class=dl-horizontal>
	<dt>0%</dt>
		<dd>-0.287590488842748</dd>
	<dt>25%</dt>
		<dd>-0.207174379971131</dd>
	<dt>50%</dt>
		<dd>-0.168666454686943</dd>
	<dt>75%</dt>
		<dd>-0.0496574140735567</dd>
	<dt>100%</dt>
		<dd>71.3982701827486</dd>
</dl>




<dl class=dl-horizontal>
	<dt>0%</dt>
		<dd>-0.287335469470005</dd>
	<dt>25%</dt>
		<dd>-0.207599412259036</dd>
	<dt>50%</dt>
		<dd>-0.160165808928844</dd>
	<dt>75%</dt>
		<dd>-0.0496574140735567</dd>
	<dt>100%</dt>
		<dd>55.2130406593281</dd>
</dl>



I have chosen quantiles (instead of the arithmetic or geometric mean) to better depict the distribution of the centered and scaled values, that have been obtained during the pre-processing phase.

## 3.3 Removing inadmissible variables from test set

Taking care of the non-predictive attributes "url" and "timedelta".


```R
nonpredictive <- c("url", "timedelta")

# removing redundant variables to (parially) limit the warning
# "prediction from a rank-deficient fit may be misleading"
# source: http://rickchungtw-blog.logdown.com/posts/957748-r-language-glm-warning-prediction-from-a-rank-deficient-fit-may-be-misleading
redundant <- c("weekday_is_sunday", "is_weekend")

# no dummyVars() is needed now, since all types are numerical (ordinal)

admissible <- names(train_processed)[!names(train_processed) %in% union(redundant,nonpredictive)]
train_processed <- train_processed[, admissible]
```

# 4. Learning Algorithms
## description of the algorithms

### Training by cross-validation

"Cross-validation, sometimes called rotation estimation, is a model validation technique for assessing how the results of a statistical analysis will **generalize to an independent data set**. It is mainly used in settings where the goal is prediction, and one wants to estimate how accurately a predictive model will perform in practice. 

In a prediction problem, a model is usually given a dataset of known data on which training is run (**training** dataset), and a dataset of unknown data (or first seen data) against which the model is tested (**testing** dataset). The goal of cross validation is to define a dataset to **"test" the model in the training phase** (i.e., the validation dataset), in order to limit problems like **overfitting**, give an insight on how the model will generalize to an independent dataset (i.e., an unknown dataset, for instance from a real problem), etc."

source: https://en.wikipedia.org/wiki/Cross-validation_(statistics)


```R
fitControl <- trainControl(## 10-fold CV
                           method = "repeatedcv",
                           number = 10,
                           ## repeated ten times
                           repeats = 10)
```


```R
library(pls, warn.conflicts = FALSE)
```

## Linear Regression with selected features


```R
model_lm_sel<-train(train_processed[,predictors],train_processed[,"shares"],method='lm', trControl = fitControl)
plot(varImp(object=model_lm_sel),main="LM (selected) - Variable Importance")
summary(model_lm_sel)
```




    
    Call:
    lm(formula = .outcome ~ ., data = dat)
    
    Residuals:
       Min     1Q Median     3Q    Max 
    -3.230 -0.186 -0.116 -0.024 71.262 
    
    Coefficients:
                                    Estimate Std. Error t value Pr(>|t|)    
    (Intercept)                    2.970e-17  5.567e-03   0.000 1.000000    
    kw_min_avg                    -4.485e-02  7.321e-03  -6.127 9.05e-10 ***
    kw_avg_avg                     2.122e-01  1.254e-02  16.927  < 2e-16 ***
    kw_max_avg                    -1.071e-01  1.118e-02  -9.579  < 2e-16 ***
    data_channel_is_entertainment -1.842e-02  5.571e-03  -3.307 0.000945 ***
    self_reference_min_shares      2.366e-02  5.600e-03   4.224 2.40e-05 ***
    num_hrefs                      3.034e-02  5.616e-03   5.403 6.59e-08 ***
    ---
    Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1
    
    Residual standard error: 0.9914 on 31708 degrees of freedom
    Multiple R-squared:  0.01727,	Adjusted R-squared:  0.01708 
    F-statistic: 92.87 on 6 and 31708 DF,  p-value: < 2.2e-16
    



    
![png](results_files/results_21_2.png)
    


## Linear Regression


```R
model_lm <- train(shares ~ ., data = train_processed, method = "lm", trControl = fitControl)
plot(varImp(object=model_lm),main="LM - Variable Importance")
summary(model_lm)
```

    Warning message in predict.lm(modelFit, newdata):
    “prediction from a rank-deficient fit may be misleading”Warning message in predict.lm(modelFit, newdata):
    “prediction from a rank-deficient fit may be misleading”Warning message in predict.lm(modelFit, newdata):
    “prediction from a rank-deficient fit may be misleading”Warning message in predict.lm(modelFit, newdata):
    “prediction from a rank-deficient fit may be misleading”Warning message in predict.lm(modelFit, newdata):
    “prediction from a rank-deficient fit may be misleading”Warning message in predict.lm(modelFit, newdata):
    “prediction from a rank-deficient fit may be misleading”Warning message in predict.lm(modelFit, newdata):
    “prediction from a rank-deficient fit may be misleading”Warning message in predict.lm(modelFit, newdata):
    “prediction from a rank-deficient fit may be misleading”Warning message in predict.lm(modelFit, newdata):
    “prediction from a rank-deficient fit may be misleading”Warning message in predict.lm(modelFit, newdata):
    “prediction from a rank-deficient fit may be misleading”




    
    Call:
    lm(formula = .outcome ~ ., data = dat)
    
    Residuals:
       Min     1Q Median     3Q    Max 
    -3.047 -0.190 -0.103 -0.010 71.182 
    
    Coefficients:
                                    Estimate Std. Error t value Pr(>|t|)    
    (Intercept)                   -1.641e-15  5.559e-03   0.000 1.000000    
    n_tokens_title                 1.709e-02  5.833e-03   2.931 0.003385 ** 
    n_tokens_content               2.633e-02  1.012e-02   2.603 0.009243 ** 
    n_unique_tokens                1.139e+00  7.284e-01   1.564 0.117791    
    n_non_stop_words              -1.145e+00  2.996e+00  -0.382 0.702413    
    n_non_stop_unique_tokens      -3.849e-01  5.733e-01  -0.671 0.501983    
    num_hrefs                      2.384e-02  7.263e-03   3.282 0.001031 ** 
    num_self_hrefs                -2.046e-02  6.575e-03  -3.112 0.001859 ** 
    num_imgs                       7.879e-03  7.117e-03   1.107 0.268286    
    num_videos                     4.873e-03  6.203e-03   0.785 0.432186    
    average_token_length          -2.746e-02  1.968e-02  -1.395 0.162940    
    num_keywords                   7.541e-03  6.823e-03   1.105 0.269051    
    data_channel_is_lifestyle     -2.331e-02  8.513e-03  -2.738 0.006181 ** 
    data_channel_is_entertainment -4.255e-02  9.381e-03  -4.536 5.76e-06 ***
    data_channel_is_bus           -3.299e-02  1.347e-02  -2.448 0.014361 *  
    data_channel_is_socmed        -1.306e-02  8.406e-03  -1.553 0.120360    
    data_channel_is_tech          -2.134e-02  1.389e-02  -1.537 0.124398    
    data_channel_is_world         -2.579e-02  1.478e-02  -1.745 0.080912 .  
    kw_min_min                     1.115e-02  1.086e-02   1.027 0.304315    
    kw_max_min                     5.670e-03  1.937e-02   0.293 0.769782    
    kw_avg_min                     6.900e-03  1.905e-02   0.362 0.717170    
    kw_min_max                    -1.113e-02  6.560e-03  -1.697 0.089706 .  
    kw_max_max                    -1.050e-02  1.193e-02  -0.880 0.378678    
    kw_avg_max                    -6.636e-03  1.078e-02  -0.615 0.538250    
    kw_min_avg                    -2.784e-02  8.254e-03  -3.373 0.000743 ***
    kw_max_avg                    -9.707e-02  1.494e-02  -6.497 8.29e-11 ***
    kw_avg_avg                     1.779e-01  1.809e-02   9.833  < 2e-16 ***
    self_reference_min_shares      2.158e-02  1.388e-02   1.555 0.120066    
    self_reference_max_shares      2.328e-02  1.607e-02   1.449 0.147416    
    self_reference_avg_sharess    -1.118e-02  2.379e-02  -0.470 0.638461    
    weekday_is_monday              6.630e-03  9.551e-03   0.694 0.487619    
    weekday_is_tuesday            -6.366e-03  9.792e-03  -0.650 0.515663    
    weekday_is_wednesday          -1.095e-03  9.818e-03  -0.112 0.911173    
    weekday_is_thursday           -8.577e-03  9.721e-03  -0.882 0.377607    
    weekday_is_friday             -6.885e-03  9.156e-03  -0.752 0.452077    
    weekday_is_saturday            1.031e-02  7.465e-03   1.381 0.167294    
    LDA_00                        -1.841e+01  1.396e+02  -0.132 0.895126    
    LDA_01                        -1.540e+01  1.167e+02  -0.132 0.894996    
    LDA_02                        -1.975e+01  1.496e+02  -0.132 0.894968    
    LDA_03                        -2.066e+01  1.566e+02  -0.132 0.895056    
    LDA_04                        -2.025e+01  1.535e+02  -0.132 0.895053    
    global_subjectivity            2.556e-02  9.541e-03   2.679 0.007382 ** 
    global_sentiment_polarity      6.192e-03  1.555e-02   0.398 0.690555    
    global_rate_positive_words    -2.227e-02  1.209e-02  -1.842 0.065424 .  
    global_rate_negative_words     1.473e-03  1.439e-02   0.102 0.918510    
    rate_positive_words            3.405e-02  9.487e-02   0.359 0.719715    
    rate_negative_words            3.116e-02  7.871e-02   0.396 0.692241    
    avg_positive_polarity         -1.069e-02  1.376e-02  -0.777 0.437342    
    min_positive_polarity         -1.296e-02  7.876e-03  -1.646 0.099805 .  
    max_positive_polarity          7.782e-03  1.029e-02   0.757 0.449320    
    avg_negative_polarity         -8.702e-03  1.547e-02  -0.563 0.573757    
    min_negative_polarity          3.341e-03  1.281e-02   0.261 0.794279    
    max_negative_polarity         -2.727e-03  9.630e-03  -0.283 0.777007    
    title_subjectivity            -8.228e-04  8.550e-03  -0.096 0.923340    
    title_sentiment_polarity       2.280e-03  6.436e-03   0.354 0.723206    
    abs_title_subjectivity         1.174e-02  6.632e-03   1.770 0.076718 .  
    abs_title_sentiment_polarity   1.473e-02  8.623e-03   1.708 0.087569 .  
    ---
    Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1
    
    Residual standard error: 0.9901 on 31658 degrees of freedom
    Multiple R-squared:  0.0215,	Adjusted R-squared:  0.01977 
    F-statistic: 12.42 on 56 and 31658 DF,  p-value: < 2.2e-16
    



    
![png](results_files/results_23_3.png)
    


## Partial Least Squares

"Partial least squares (PLS) regression is a technique that **reduces the predictors** to a smaller set of **uncorrelated** components and performs **least squares regression** on these components, instead of on the original data. PLS regression is especially useful when your predictors are highly collinear, or when you have more predictors than observations and ordinary least-squares regression either produces coefficients with high standard errors or fails completely."

source: 
http://support.minitab.com/en-us/minitab/17/topic-library/modeling-statistics/regression-and-correlation/partial-least-squares-regression/what-is-partial-least-squares-regression/

"PLS combines features of principal components analysis and multiple regression. It first extracts a set of latent factors that explain as much of the **covariance** as possible between the **independent and dependent variables**. Then a regression step predicts values of the dependent variables using the **decomposition** of the **independent variables**."

source: https://www.ibm.com/support/knowledgecenter/SSLVMB_22.0.0/com.ibm.spss.statistics.help/spss/base/idh_idd_pls_variables.htm

*ABDI, Hervé. Partial least square regression (PLS regression). Encyclopedia for research methods for the social sciences, 2003, 792-795.*


```R
model_pls <- train(shares ~ ., data = train_processed, method = "pls", trControl = fitControl)
plot(varImp(object=model_pls),main="PLS - Variable Importance")
summary(model_pls)
```



    Data: 	X dimension: 31715 56 
    	Y dimension: 31715 1
    Fit method: oscorespls
    Number of components considered: 3
    TRAINING: % variance explained
              1 comps  2 comps  3 comps
    X           6.608   11.085   16.836
    .outcome    1.501    1.768    1.863
    


    
![png](results_files/results_25_2.png)
    


## Principal Component Analysis (PCA)
"Principal components analysis is a procedure for identifying a smaller number of uncorrelated variables, called "principal components", from a large set of data. The goal of principal components analysis is to **explain the maximum amount of variance** with the **fewest** number of **principal components**. Principal components analysis is commonly used in the **social sciences**, market research, and other industries that use large data sets."

source: http://support.minitab.com/en-us/minitab/17/topic-library/modeling-statistics/multivariate/principal-components-and-factor-analysis/what-is-pca/

"Principal component analysis (PCA) is a statistical procedure that uses an orthogonal transformation to convert a set of observations of **possibly correlated** variables into a set of values of **linearly uncorrelated** variables called **principal components** (or sometimes, principal modes of variation). [...] This transformation is defined in such a way that the **first principal component** has the **largest possible variance** (that is, accounts for as much of the variability in the data as possible), and each succeeding component in turn has the highest variance possible under the constraint that it is orthogonal to the preceding components. The resulting vectors are an uncorrelated orthogonal basis set. PCA is **sensitive** to the **relative scaling** of the original variables."

source: https://en.wikipedia.org/wiki/Principal_component_analysis

*BRO, Rasmus; SMILDE, Age K. Principal component analysis. Analytical Methods, 2014, 6.9: 2812-2831.*

### The difference between PCR and PCA
"[...] principal component regression (PCR) is a regression analysis that uses principal component analysis to create explanatory variables before estimating regression coefficients. In PCR instead of regressing the dependent variable on the independent variables directly, the principal components of the independent variables are used."

source: https://stats.stackexchange.com/a/55984
source: https://en.wikipedia.org/wiki/Principal_component_regression


```R
model_pcr <- train(shares ~ ., data = train_processed, method = "pcr", trControl = fitControl)
plot(varImp(object=model_pcr),main="PCR - Variable Importance")
summary(model_pcr)
```



    Data: 	X dimension: 31715 56 
    	Y dimension: 31715 1
    Fit method: svdpc
    Number of components considered: 3
    TRAINING: % variance explained
              1 comps  2 comps  3 comps
    X         8.54297  15.6768    22.12
    .outcome  0.08482   0.4467     1.05
    


    
![png](results_files/results_28_2.png)
    


# 5. Evaluation

## Linear Regression with selected features


```R
## Linear Regression
predictions <- predict.train(object=model_lm_sel, test_processed,type="raw")
postResample(pred = predictions, obs = test_processed$shares)

comparison <- data.frame(head(predictions), head(test_processed$shares))
colnames(comparison) <- c("prediction", "real value")
comparison

## Linear Regression (training set)
predictions_train <- predict.train(object=model_lm_sel, train_processed,type="raw")
postResample(pred = predictions_train, obs = train_processed$shares)

comparison <- data.frame(head(predictions_train), head(test_processed$shares))
colnames(comparison) <- c("prediction", "real value")
comparison
```


<dl class=dl-horizontal>
	<dt>RMSE</dt>
		<dd>0.929129123507746</dd>
	<dt>Rsquared</dt>
		<dd>0.0240434139430349</dd>
</dl>




<table>
<thead><tr><th></th><th scope=col>prediction</th><th scope=col>real value</th></tr></thead>
<tbody>
	<tr><th scope=row>10</th><td>-0.3759586 </td><td>-0.22732091</td></tr>
	<tr><th scope=row>13</th><td>-0.3253678 </td><td>-0.21771518</td></tr>
	<tr><th scope=row>18</th><td>-0.3574563 </td><td>-0.02415548</td></tr>
	<tr><th scope=row>23</th><td>-0.3329757 </td><td>-0.24984762</td></tr>
	<tr><th scope=row>32</th><td>-0.3812450 </td><td> 0.04384969</td></tr>
	<tr><th scope=row>33</th><td>-0.3865313 </td><td>-0.24687240</td></tr>
</tbody>
</table>




<dl class=dl-horizontal>
	<dt>RMSE</dt>
		<dd>0.991311364500452</dd>
	<dt>Rsquared</dt>
		<dd>0.0172707923531425</dd>
</dl>




<table>
<thead><tr><th></th><th scope=col>prediction</th><th scope=col>real value</th></tr></thead>
<tbody>
	<tr><th scope=row>11401</th><td>-0.06962899</td><td>-0.22732091</td></tr>
	<tr><th scope=row>31251</th><td>-0.08182251</td><td>-0.21771518</td></tr>
	<tr><th scope=row>16213</th><td> 0.11086790</td><td>-0.02415548</td></tr>
	<tr><th scope=row>35004</th><td> 0.14076687</td><td>-0.24984762</td></tr>
	<tr><th scope=row>37281</th><td> 0.21386069</td><td> 0.04384969</td></tr>
	<tr><th scope=row>1806</th><td>-0.15298176</td><td>-0.24687240</td></tr>
</tbody>
</table>



## Linear Regression


```R
## Linear Regression
predictions <- predict.train(object=model_lm, test_processed,type="raw")
postResample(pred = predictions, obs = test_processed$shares)

comparison <- data.frame(head(predictions), head(test_processed$shares))
colnames(comparison) <- c("prediction", "real value")
comparison

## Linear Regression (training set)
predictions_train <- predict.train(object=model_lm, train_processed,type="raw")
postResample(pred = predictions_train, obs = train_processed$shares)

comparison <- data.frame(head(predictions_train), head(test_processed$shares))
colnames(comparison) <- c("prediction", "real value")
comparison
```


<dl class=dl-horizontal>
	<dt>RMSE</dt>
		<dd>0.927741970579579</dd>
	<dt>Rsquared</dt>
		<dd>0.0267864821715714</dd>
</dl>




<table>
<thead><tr><th></th><th scope=col>prediction</th><th scope=col>real value</th></tr></thead>
<tbody>
	<tr><th scope=row>10</th><td>-0.35154697</td><td>-0.22732091</td></tr>
	<tr><th scope=row>13</th><td>-0.08140852</td><td>-0.21771518</td></tr>
	<tr><th scope=row>18</th><td>-0.29504271</td><td>-0.02415548</td></tr>
	<tr><th scope=row>23</th><td>-0.27750698</td><td>-0.24984762</td></tr>
	<tr><th scope=row>32</th><td>-0.25109148</td><td> 0.04384969</td></tr>
	<tr><th scope=row>33</th><td>-0.19380770</td><td>-0.24687240</td></tr>
</tbody>
</table>




<dl class=dl-horizontal>
	<dt>RMSE</dt>
		<dd>0.989175587495601</dd>
	<dt>Rsquared</dt>
		<dd>0.0215008042193715</dd>
</dl>




<table>
<thead><tr><th></th><th scope=col>prediction</th><th scope=col>real value</th></tr></thead>
<tbody>
	<tr><th scope=row>11401</th><td>-0.12501032</td><td>-0.22732091</td></tr>
	<tr><th scope=row>31251</th><td>-0.18689619</td><td>-0.21771518</td></tr>
	<tr><th scope=row>16213</th><td> 0.16955901</td><td>-0.02415548</td></tr>
	<tr><th scope=row>35004</th><td> 0.10959417</td><td>-0.24984762</td></tr>
	<tr><th scope=row>37281</th><td> 0.17915365</td><td> 0.04384969</td></tr>
	<tr><th scope=row>1806</th><td>-0.08612749</td><td>-0.24687240</td></tr>
</tbody>
</table>



## Partial Least Squares


```R
## Partial Least Squares
predictions <- predict.train(object=model_pls, test_processed,type="raw")
postResample(pred = predictions, obs = test_processed$shares)

comparison <- data.frame(head(predictions), head(test_processed$shares))
colnames(comparison) <- c("prediction", "real value")
comparison

## Partial Least Squares (training set)
predictions_train <- predict.train(object=model_pls, train_processed,type="raw")
postResample(pred = predictions_train, obs = train_processed$shares)

comparison <- data.frame(head(predictions_train), head(test_processed$shares))
colnames(comparison) <- c("prediction", "real value")
comparison
```


<dl class=dl-horizontal>
	<dt>RMSE</dt>
		<dd>0.929004766351723</dd>
	<dt>Rsquared</dt>
		<dd>0.0241509063590263</dd>
</dl>




<table>
<thead><tr><th scope=col>prediction</th><th scope=col>real value</th></tr></thead>
<tbody>
	<tr><td>-0.25851774</td><td>-0.22732091</td></tr>
	<tr><td> 0.04041799</td><td>-0.21771518</td></tr>
	<tr><td>-0.16838833</td><td>-0.02415548</td></tr>
	<tr><td>-0.11477003</td><td>-0.24984762</td></tr>
	<tr><td>-0.09821745</td><td> 0.04384969</td></tr>
	<tr><td>-0.07743001</td><td>-0.24687240</td></tr>
</tbody>
</table>




<dl class=dl-horizontal>
	<dt>RMSE</dt>
		<dd>0.990627800170637</dd>
	<dt>Rsquared</dt>
		<dd>0.0186256179887404</dd>
</dl>




<table>
<thead><tr><th scope=col>prediction</th><th scope=col>real value</th></tr></thead>
<tbody>
	<tr><td>-0.10375211</td><td>-0.22732091</td></tr>
	<tr><td>-0.13010343</td><td>-0.21771518</td></tr>
	<tr><td> 0.27121233</td><td>-0.02415548</td></tr>
	<tr><td> 0.10352012</td><td>-0.24984762</td></tr>
	<tr><td> 0.20443143</td><td> 0.04384969</td></tr>
	<tr><td>-0.06776604</td><td>-0.24687240</td></tr>
</tbody>
</table>



## Principal Component Analysis


```R
## Principal Component Analysis
predictions <- predict.train(object=model_pcr, test_processed,type="raw")
postResample(pred = predictions, obs = test_processed$shares)

comparison <- data.frame(head(predictions), head(test_processed$shares))
colnames(comparison) <- c("prediction", "real value")
comparison

## Principal Component Analysis (training set)
predictions_train <- predict.train(object=model_pcr, train_processed,type="raw")
postResample(pred = predictions_train, obs = train_processed$shares)

comparison <- data.frame(head(predictions_train), head(test_processed$shares))
colnames(comparison) <- c("prediction", "real value")
comparison
```


<dl class=dl-horizontal>
	<dt>RMSE</dt>
		<dd>0.933674583148104</dd>
	<dt>Rsquared</dt>
		<dd>0.0144136244600339</dd>
</dl>




<table>
<thead><tr><th scope=col>prediction</th><th scope=col>real value</th></tr></thead>
<tbody>
	<tr><td>-0.23953620</td><td>-0.22732091</td></tr>
	<tr><td>-0.09341019</td><td>-0.21771518</td></tr>
	<tr><td>-0.16406211</td><td>-0.02415548</td></tr>
	<tr><td>-0.15103334</td><td>-0.24984762</td></tr>
	<tr><td>-0.18398709</td><td> 0.04384969</td></tr>
	<tr><td>-0.17384785</td><td>-0.24687240</td></tr>
</tbody>
</table>




<dl class=dl-horizontal>
	<dt>RMSE</dt>
		<dd>0.994719513568607</dd>
	<dt>Rsquared</dt>
		<dd>0.0105018896376621</dd>
</dl>




<table>
<thead><tr><th scope=col>prediction</th><th scope=col>real value</th></tr></thead>
<tbody>
	<tr><td> 0.04483332</td><td>-0.22732091</td></tr>
	<tr><td>-0.08474661</td><td>-0.21771518</td></tr>
	<tr><td> 0.01857546</td><td>-0.02415548</td></tr>
	<tr><td> 0.04191770</td><td>-0.24984762</td></tr>
	<tr><td> 0.17025973</td><td> 0.04384969</td></tr>
	<tr><td>-0.19506830</td><td>-0.24687240</td></tr>
</tbody>
</table>



# 6. Conclusion
Interestingly, in all models the predictions on the train set are outperformed by the prediction on the test set, although only marginally. However, this is subject to change if different random seed is used. Yet again, the difference is marginal and consistent accross models. This is not inconsistent with theory nor overfitting, as overfitting is defined as follows. 

"A hypothesis, h, is said to overfit the training data is there exists another hypothesis which, h´, such that h has **less error** than h´ on the **training** data but **greater error** on independent **test** data."

However, my case is quite the contrary, which might indicate generalization.

Apart from slight changes in order, the most important variables have been chosen consistently accross models. I also investigated a case with Linear Regression using ONLY the selected (most important) variables to see how it differs from a Linear Regression on all variables. The difference was extremely negligible, with slight preference towards the "full" Linear Regression model, indeed.

Due to a limited hardware (and discretionality of the requirement), the formal Feature selection has been omited in favour of the selection of statistically significant variables by each model. However, the code has been included.

For better readability and performance, only the most important variables have been visualised. I have preferred quartiles instead of range to demonstrate the variance of news$shares.
